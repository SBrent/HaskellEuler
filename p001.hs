mulSum35 :: Int -> Int
mulSum35 i = sum [j | j <- [0..i-1], mod j 3 == 0 || mod j 5 == 0]

main = print $ mulSum35 1000

